#!/bin/bash

source entrypoint.sh

echo "Running rosrun rviz rviz for mapping..."

rosparam set use_sim_time true
rosrun rviz rviz -d /rviz-configs/rviz_custom_slam_nav.rviz

