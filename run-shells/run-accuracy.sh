#!/bin/bash

source entrypoint.sh

echo "Running rosrun rviz rviz..."

rosparam set use_sim_time true
rosrun rviz rviz -d /rviz-configs/accuracy_test.rviz

