#!/bin/bash

source entrypoint.sh

echo "Running rosrun rviz rviz for exploration..."

rosparam set use_sim_time true
rosrun rviz rviz -d /rviz-configs/rviz_exploration.rviz

