# Simulation Environment for Baxter's Mobility Base using HW Accelerated Container

![Gazebo Simulator Running in HW accelerated container](/pics/mb-gazebo-rqt-topics-graph.png?raw=true "Gazebo")


## Prerequisites
The graphical components or simulation runs inside a (optimally HW accelerated) Docker container that is available from the [internal registry](https://tbd.com). To allow hardware acceleration using a NVIDIA card install the required Container Runtime following the guide at [NVIDIA/nvidia-docker](https://github.com/NVIDIA/nvidia-docker). 

In order to run nvidia-docker2 you should check that the following versions are present on your system:
1. GNU/Linux x86_64 with kernel version > 3.10
2. Docker >= 1.12
3. NVIDIA GPU with Architecture > Fermi (2.1)
4. NVIDIA drivers ~= 361.93

The steps to installing the prerequisites needed are (for Linux)

1. Install CUDA drivers as documented [here](https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#package-manager-installation)
2. Install nvidia-docker2 as described [here](https://github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0))

## Run simulation
Get image from [RepoHub](repohub.enterpriselab.ch:5000) 
```
docker login -u <USER> -p <PASS> repohub.enterpriselab.ch:5002
docker pull repohub.enterpriselab.ch:5002/kawa/ros-kinetic-mb-sim-gl:latest
```

or build yourself by cloning and running

```
docker build -t ros-kinetic-mb-sim-gl:latest .
```

Once the container image is available locally set the following variables to allow X11 forwarding of windowed applications from within containers to the host X server

```
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth

touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
```

To launch Gazebo in the example world run

```
docker run -it --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" --env="XAUTHORITY=$XAUTH" --volume="$XAUTH:$XAUTH" --runtime=nvidia --name=mb-sim --network=host ros-kinetic-mb-sim-gl bash run.sh
```

or to run any graphical program inside that container run 

```
docker run -it --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" --env="XAUTHORITY=$XAUTH" --volume="$XAUTH:$XAUTH" --runtime=nvidia --name=mb-sim --network=host ros-kinetic-mb-sim-gl bash
```

Wait for container `bash` to appear then run

```
roslaunch mobility_base_gazebo mobility_base_custom.launch
```

### RViz with Robot Description

In a seperate Terminal run these assignments again

```
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth

touch $XAUTH
xauth nlist $DISPLAY | sed -e 's/^..../ffff/' | xauth -f $XAUTH nmerge -
```

![RViz running inside seperate container with robot description enabled](/pics/mb-rviz-robot-description.png?raw=true "RViz")

Then start a seperate container using

```
docker run -it --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" --env="XAUTHORITY=$XAUTH" --volume="$XAUTH:$XAUTH" --runtime=nvidia --name=mb-sim-rviz --network=host ros-kinetic-mb-sim-gl bash
```

Wait for container `bash` to appear then run

```
rosrun rviz rviz
```

