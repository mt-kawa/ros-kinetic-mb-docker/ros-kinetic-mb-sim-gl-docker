FROM osrf/ros:kinetic-desktop-full
MAINTAINER Kai Waelti <Kai.Waelti@dfki.de>

RUN apt-get update && apt-get install -y --no-install-recommends \
    pkg-config \
    libxau-dev \
    libxdmcp-dev \
    libxcb1-dev \
    libxext-dev \
    libx11-dev \
    libglu1-mesa-dev \
    mesa-utils \
    curl \
    ca-certificates \
    xterm \
    kmod \
    wget \
    software-properties-common && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get remove -y ros-kinetic-gazebo* && apt-get -y upgrade

RUN sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list' && \
    wget http://packages.osrfoundation.org/gazebo.key -O - | apt-key add - && \
    apt-get update && \
    apt-get install --no-install-recommends -y ros-kinetic-gazebo8-ros-pkgs ros-kinetic-gazebo8-ros-control ros-kinetic-gazebo8*

COPY --from=nvidia/opengl:1.0-glvnd-runtime-ubuntu16.04 \
  /usr/local/lib/x86_64-linux-gnu \
  /usr/local/lib/x86_64-linux-gnu

COPY --from=nvidia/opengl:1.0-glvnd-runtime-ubuntu16.04 \
  /usr/local/share/glvnd/egl_vendor.d/10_nvidia.json \
  /usr/local/share/glvnd/egl_vendor.d/10_nvidia.json

RUN echo '/usr/local/lib/x86_64-linux-gnu' >> /etc/ld.so.conf.d/glvnd.conf && \
    ldconfig && \
    echo '/usr/local/$LIB/libGL.so.1' >> /etc/ld.so.preload && \
    echo '/usr/local/$LIB/libEGL.so.1' >> /etc/ld.so.preload

# nvidia-container-runtime
ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

RUN apt-get update && apt-get install -q -y \
    wget \
    && rm -rf /var/lib/apt/lists/*

RUN echo "Adding Dataspeed server to apt..." && \
    sh -c 'echo "deb [ arch=amd64 ] http://packages.dataspeedinc.com/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-dataspeed-public.list' && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 66F84AE1EB71A8AC108087DCAF677210FF6D3CDA && \
    apt-get update && \
    echo "Setting up rosdep..." && \
    sh -c 'echo "yaml http://packages.dataspeedinc.com/ros/ros-public-kinetic.yaml kinetic" > /etc/ros/rosdep/sources.list.d/30-dataspeed-public-kinetic.list' && \
    rosdep update && \
    apt-get update && apt-get install -q -y ros-kinetic-mobility-base ros-kinetic-mobility-base-driver && \
    apt-get upgrade -y && \
    echo "SDK install: Done"

SHELL ["/bin/bash", "-c"]	# Change to bash shell for ros stuff

COPY mobility_base.rosinstall /tmp/
COPY mobility_base_simulator.rosinstall /tmp/

# Install mobility_base_simulator packages into catkin ws
RUN mkdir -p /root/catkin_ws && \
    cd /root/catkin_ws && \
    wstool init src && \
    wstool merge -t src /tmp/mobility_base.rosinstall && \
    wstool merge -t src /tmp/mobility_base_simulator.rosinstall && \
    wstool update -t src

WORKDIR /root/catkin_ws

RUN rm -rf src/mobility_base_simulator

RUN source /opt/ros/kinetic/setup.bash && \
    catkin_make -j2 -DCMAKE_BUILD_TYPE=Release

# To make local changes to the mobility_base_simulator package uncomment this
# and comment he following RUN command
# COPY ./src/mobility_base_simulator src/mobility_base_simulator

RUN git clone https://github.com/kw90/mobility_base_simulator.git src/mobility_base_simulator && \
    source /root/catkin_ws/devel/setup.bash && \
    catkin_make -j2 -DCMAKE_BUILD_TYPE=Release

COPY /urdf/mobility_base.urdf.xacro /root/catkin_ws/src/mobility_base_ros/mobility_base_description/urdf/mobility_base.urdf.xacro
COPY /urdf/Hokuyo_URG-04LX.urdf.xacro /root/catkin_ws/src/mobility_base_ros/mobility_base_description/urdf/sensors/Hokuyo_URG-04LX.urdf.xacro
COPY /urdf/Hokuyo_UST-10LX.urdf.xacro /root/catkin_ws/src/mobility_base_ros/mobility_base_description/urdf/sensors/Hokuyo_UST-10LX.urdf.xacro
COPY /urdf/Hokuyo_UST-20LX.urdf.xacro /root/catkin_ws/src/mobility_base_ros/mobility_base_description/urdf/sensors/Hokuyo_UST-20LX.urdf.xacro
COPY run-shells /run-shells
COPY sim-config_launch /
COPY rviz-configs /rviz-configs
COPY worlds /usr/share/gazebo-8/worlds

# RUN source /root/catkin_ws/devel/setup.bash && \
#     gzserver --verbose --iters 1 /usr/share/gazebo-8/worlds/workshop-v5-g8.world

WORKDIR /root

COPY entrypoint.sh .

# nvidia-docker links
LABEL com.nvidia.volumes.needed="nvidia_driver"
ENV PATH /usr/local/nvidia/bin:/opt/VirtualGL/bin:${PATH}
ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64:${LD_LIBRARY_PATH}

ENTRYPOINT ["/root/entrypoint.sh"]

ENV ROS_MASTER_URI "http://ros-master:11311"

CMD ["bash"]

